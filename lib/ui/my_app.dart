import 'package:contact_list_challenge/ui/app_theme.dart';
import 'package:flutter/material.dart';

import 'app_router.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      darkTheme: AppTheme().dark,
      routerDelegate: AppRouter().routerDelegate,
      routeInformationParser: AppRouter().routeInformationParser,
      routeInformationProvider: AppRouter().routeInformationProvider,
      theme: AppTheme().light,
      title: 'Contact List Challenge',
    );
  }
}
