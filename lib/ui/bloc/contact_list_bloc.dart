import '../../data/models/contact.dart';
import '../../data/repositories/contact_repository.dart';
import 'bloc.dart';

class ContactListBloc extends Bloc<List<Contact>> {
  final _repository = ContactRepository();

  ContactListBloc({
    required super.builder,
    super.initialData,
    super.key,
  }) {
    fetch();
  }

  Future<void> fetch() async {
    if (initialData != null) {
      return;
    }

    try {
      final items = await _repository.all;
      sink.add(items);
    } catch (e) {
      sink.addError(e);
    }
  }
}
