import 'dart:async';

import 'package:flutter/material.dart';

class Bloc<T> extends StatefulWidget {
  final Widget Function(BuildContext, AsyncSnapshot<T>) builder;
  final T? initialData;
  final _streamController = StreamController<T>.broadcast();
  late final sink = _streamController.sink;

  Bloc({
    required this.builder,
    this.initialData,
    Key? key,
  }) : super(key: key);

  @override
  State<Bloc<T>> createState() => _BlocState<T>();
}

class _BlocState<T> extends State<Bloc<T>> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      builder: widget.builder,
      initialData: widget.initialData,
      stream: widget._streamController.stream,
    );
  }

  @override
  void dispose() {
    widget._streamController.close();
    super.dispose();
  }
}
