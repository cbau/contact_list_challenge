import '../../data/models/contact.dart';
import '../../data/repositories/contact_repository.dart';
import 'bloc.dart';

class ContactDetailsBloc extends Bloc<Contact> {
  final int? itemId;
  final _repository = ContactRepository();

  ContactDetailsBloc({
    required super.builder,
    super.initialData,
    this.itemId,
    super.key,
  }) {
    fetch();
  }

  Future<void> fetch() async {
    if (initialData != null) {
      return;
    }

    if (itemId == null) {
      sink.addError(Exception('Item or identifier must be specified.'));
    } else {
      try {
        final newItem = await _repository.get(itemId!);
        if (newItem != null) {
          sink.add(newItem);
        } else {
          sink.addError(Exception('Contact not found'));
        }
      } catch (e) {
        sink.addError(e);
      }
    }
  }
}
