import 'package:contact_list_challenge/ui/pages/contact_list_page.dart';
import 'package:flutter/material.dart';

class ContactPage extends StatelessWidget {
  static const pageName = 'Contacts';

  final Widget? child;

  const ContactPage({
    this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final isSinglePage = constraints.minWidth < 600;
      final isDetailsPage = isSinglePage && child != null;

      return Scaffold(
        appBar: AppBar(
          title: isDetailsPage ? const Text('Contact') : const Text('Contacts'),
        ),
        body: isSinglePage
            ? child ?? const ContactListPage()
            : Row(
                children: [
                  const Expanded(
                    child: ContactListPage(),
                  ),
                  Expanded(
                    child: child ??
                        const Center(
                          child: Text('Select a contact to see the details'),
                        ),
                  ),
                ],
              ),
      );
    });
  }
}
