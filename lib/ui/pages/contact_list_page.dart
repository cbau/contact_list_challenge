import 'package:flutter/material.dart';

import '../bloc/contact_list_bloc.dart';
import '../widgets/contact_item.dart';

class ContactListPage extends StatelessWidget {
  const ContactListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContactListBloc(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemBuilder: (context, index) {
              return ContactItem(
                item: snapshot.data![index],
              );
            },
            itemCount: snapshot.data!.length,
          );
        }

        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }

        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
