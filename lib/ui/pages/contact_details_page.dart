import 'package:contact_list_challenge/ui/widgets/circle_cached_image.dart';
import 'package:contact_list_challenge/ui/widgets/contact_details_item.dart';
import 'package:flutter/material.dart';

import '../../data/models/contact.dart';
import '../bloc/contact_details_bloc.dart';

class ContactDetailsPage extends StatelessWidget {
  static const pageName = 'ContactDetails';
  final Contact? item;
  final String itemId;

  const ContactDetailsPage({
    required this.itemId,
    this.item,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContactDetailsBloc(
      builder: (context, snapshot) {
        final textTheme = Theme.of(context).textTheme;

        if (snapshot.hasData) {
          return ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Hero(
                  tag: 'contact${snapshot.data!.id}',
                  child: CircleCachedImage(
                    radius: 80,
                    imageUrl: snapshot.data!.imageUrl,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      snapshot.data!.name,
                      style: textTheme.titleLarge,
                    ),
                    Text(
                      snapshot.data!.username,
                      style: textTheme.subtitle2,
                    ),
                  ],
                ),
              ),
              ContactDetailsItem(
                snapshot.data!.phone,
                caption: 'Phone',
                leading: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.phone),
                ),
              ),
              ContactDetailsItem(
                snapshot.data!.email,
                caption: 'E-mail',
                leading: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.email),
                ),
              ),
              ContactDetailsItem(
                snapshot.data!.website,
                caption: 'Website',
                leading: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.link),
                ),
              ),
              ContactDetailsItem(
                '${snapshot.data!.address.street} ${snapshot.data!.address.suite}\n${snapshot.data!.address.city}\n${snapshot.data!.address.zipcode}',
                caption: 'Address',
                leading: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.home),
                ),
              ),
              ContactDetailsItem(
                '${snapshot.data!.company.name}\n${snapshot.data!.company.catchPhrase}\n${snapshot.data!.company.bs}',
                caption: 'Company',
                leading: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.business),
                ),
              ),
            ],
          );
        }

        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        }

        return const Center(
          child: CircularProgressIndicator(),
        );
      },
      initialData: item,
      itemId: int.tryParse(itemId) ?? 0,
    );
  }
}
