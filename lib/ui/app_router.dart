import 'package:contact_list_challenge/ui/pages/contact_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'pages/contact_details_page.dart';
import 'pages/not_found_page.dart';

class AppRouter {
  static final AppRouter _instance = AppRouter._();
  //final _contactPageKey = const ValueKey('contactPage');

  AppRouter._();

  factory AppRouter() => _instance;

  late final _router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    routes: [
      GoRoute(
        path: '/contacts',
        redirect: (state) => '/',
      ),
      GoRoute(
        name: ContactPage.pageName,
        pageBuilder: (context, state) => const MaterialPage<void>(
          child: ContactPage(),
        ),
        path: '/',
      ),
      GoRoute(
        name: ContactDetailsPage.pageName,
        pageBuilder: (context, state) => MaterialPage<void>(
          child: ContactPage(
            child: ContactDetailsPage(
              itemId: state.params['id'] ?? '',
            ),
          ),
        ),
        path: '/contacts/:id',
      ),
    ],
  );

  RouterDelegate<Object> get routerDelegate => _router.routerDelegate;

  RouteInformationParser<Object> get routeInformationParser =>
      _router.routeInformationParser;

  RouteInformationProvider get routeInformationProvider =>
      _router.routeInformationProvider;
}
