import 'package:flutter/material.dart';

class AppTheme {
  ThemeData? _dark;
  ThemeData? _light;

  static final AppTheme _instance = AppTheme._();

  AppTheme._();

  factory AppTheme() => _instance;

  ThemeData get dark => _dark ??= _build(isDark: true);

  ThemeData get light => _light ??= _build(isDark: false);

  ThemeData _build({required bool isDark}) {
    return ThemeData(
      brightness: isDark ? Brightness.dark : Brightness.light,
      primarySwatch: Colors.blue,
    );
  }
}
