import 'package:flutter/material.dart';

class ContactDetailsItem extends StatelessWidget {
  final String? caption;
  final String details;
  final Widget? leading;

  const ContactDetailsItem(
    this.details, {
    this.caption,
    this.leading,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (leading != null) leading!,
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (caption != null)
                Text(
                  caption!,
                  style: Theme.of(context).textTheme.caption,
                ),
              Text(details),
            ],
          ),
        ),
      ],
    );
  }
}
