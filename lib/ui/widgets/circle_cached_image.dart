import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CircleCachedImage extends StatelessWidget {
  final String imageUrl;
  final double radius;

  const CircleCachedImage({
    required this.imageUrl,
    this.radius = 20.0,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: radius * 2,
      width: radius * 2,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(imageUrl),
        ),
        shape: BoxShape.circle,
      ),
    );
  }
}
