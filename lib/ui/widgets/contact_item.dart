import 'package:contact_list_challenge/ui/widgets/circle_cached_image.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../data/models/contact.dart';
import '../pages/contact_details_page.dart';

class ContactItem extends StatelessWidget {
  final Contact item;

  const ContactItem({
    required this.item,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => GoRouter.of(context).pushNamed(
        ContactDetailsPage.pageName,
        params: {'id': item.id.toString()},
        extra: item,
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Hero(
              tag: 'contact${item.id}',
              child: CircleCachedImage(
                imageUrl: item.imageUrl,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.name,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                item.company.name,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
