import 'package:sembast/sembast.dart';

import '../local.dart';
import '../models/contact.dart';

class ContactDao {
  static final _instance = ContactDao._();
  final _store = Local().contactStore;

  ContactDao._();

  factory ContactDao() => _instance;

  Future<List<Contact>> get all async {
    final database = await Local().database;
    final snapshot = await _store.query().getSnapshots(database);
    return snapshot.map((e) => Contact.fromMap(e.value)).toList();
  }

  Future<void> clear() async {
    final database = await Local().database;
    await _store.delete(database);
  }

  Future<void> delete(Contact item) async {
    final database = await Local().database;
    await _store.record(item.id).delete(database);
  }

  Future<Contact?> get(int id) async {
    final database = await Local().database;
    final map = await _store.record(id).get(database);
    return Contact.fromMap(map);
  }

  Future<void> put(Contact item) async {
    final database = await Local().database;
    await _store.record(item.id).put(database, item.toMap());
  }

  Future<void> putAll(List<Contact> items) async {
    for (final item in items) {
      await put(item);
    }
  }
}
