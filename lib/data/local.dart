import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast_web/sembast_web.dart';

class Local {
  static final _instance = Local._();

  final contactStore = intMapStoreFactory.store('contacts');

  Database? _database;

  Local._();

  factory Local() => _instance;

  Future<Database> get database async {
    if (_database == null) {
      const fileName = 'appdb.db';
      if (kIsWeb) {
        _database = await (databaseFactoryWeb).openDatabase(fileName);
      } else {
        final dir = await getApplicationDocumentsDirectory();
        await dir.create(recursive: true);
        final path = join(dir.path, fileName);
        _database = await (databaseFactoryIo).openDatabase(path);
      }
    }
    return _database!;
  }

  Future<void> clearAll() async {
    final db = await database;
    await contactStore.delete(db);
  }
}
