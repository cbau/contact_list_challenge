import 'package:http/http.dart';

class JsonPlaceholderService {
  static const _instance = JsonPlaceholderService._();

  final serviceOrigin = 'https://jsonplaceholder.typicode.com';

  const JsonPlaceholderService._();

  factory JsonPlaceholderService() => _instance;

  Future<Response> getUsers() => get(Uri.parse('$serviceOrigin/users'));
}
