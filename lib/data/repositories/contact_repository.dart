import 'dart:convert';

import '../dao/contact_dao.dart';
import '../models/contact.dart';
import '../services/json_placeholder_service.dart';

class ContactRepository {
  static final _instance = ContactRepository._();
  final _dao = ContactDao();

  ContactRepository._();

  factory ContactRepository() => _instance;

  Future<List<Contact>> get all async {
    var items = await _dao.all;

    if (items.isEmpty) {
      final response = await JsonPlaceholderService().getUsers();
      final list = json.decode(response.body) as List<Object?>;
      items =
          list.map((e) => Contact.fromMap(e as Map<String, Object?>?)).toList();
      _dao.clear();
      _dao.putAll(items);
    }

    return items;
  }

  Future<Contact?> get(int id) async {
    return await _dao.get(id);
  }
}
