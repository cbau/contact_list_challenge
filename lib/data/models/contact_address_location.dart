class ContactAddressLocation {
  final String latitude;
  final String longitude;

  const ContactAddressLocation({
    required this.latitude,
    required this.longitude,
  });

  ContactAddressLocation.fromMap(Map<String, Object?>? map)
      : this(
          latitude: map?['lat'] as String? ?? '',
          longitude: map?['lng'] as String? ?? '',
        );

  Map<String, Object> toMap() {
    return {
      'lat': latitude,
      'lng': longitude,
    };
  }
}
