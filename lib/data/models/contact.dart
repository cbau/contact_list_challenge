import 'dart:convert';

import 'package:crypto/crypto.dart';

import 'contact_address.dart';
import 'contact_company.dart';

class Contact {
  final ContactAddress address;
  final ContactCompany company;
  final String email;
  final String hash;
  final int id;
  final String name;
  final String phone;
  final String username;
  final String website;

  const Contact({
    required this.address,
    required this.company,
    required this.email,
    required this.hash,
    required this.id,
    required this.name,
    required this.phone,
    required this.username,
    required this.website,
  });

  Contact.fromMap(Map<String, Object?>? map)
      : this(
          address:
              ContactAddress.fromMap(map?['address'] as Map<String, Object?>?),
          company:
              ContactCompany.fromMap(map?['company'] as Map<String, Object?>?),
          email: map?['email'] as String? ?? '',
          id: map?['id'] as int? ?? 0,
          hash: md5
              .convert(utf8.encode(map?['email'] as String? ?? ''))
              .toString(),
          name: map?['name'] as String? ?? '',
          phone: map?['phone'] as String? ?? '',
          username: map?['username'] as String? ?? '',
          website: map?['website'] as String? ?? '',
        );

  String get imageUrl =>
      'https://www.gravatar.com/avatar/$hash?d=wavatar&s=120';

  Map<String, Object?> toMap() {
    return {
      'address': address.toMap(),
      'company': company.toMap(),
      'email': email,
      'id': id,
      'name': name,
      'phone': phone,
      'username': username,
      'website': website,
    };
  }
}
