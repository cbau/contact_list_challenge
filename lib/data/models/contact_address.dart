import 'contact_address_location.dart';

class ContactAddress {
  final String city;
  final ContactAddressLocation geo;
  final String street;
  final String suite;
  final String zipcode;

  const ContactAddress({
    required this.city,
    required this.geo,
    required this.street,
    required this.suite,
    required this.zipcode,
  });

  ContactAddress.fromMap(Map<String, Object?>? map)
      : this(
          city: map?['city'] as String? ?? '',
          geo: ContactAddressLocation.fromMap(
              map?['geo'] as Map<String, Object?>?),
          street: map?['street'] as String? ?? '',
          suite: map?['suite'] as String? ?? '',
          zipcode: map?['zipcode'] as String? ?? '',
        );

  Map<String, Object?> toMap() {
    return {
      'city': city,
      'geo': geo.toMap(),
      'street': street,
      'suite': suite,
      'zipcode': zipcode,
    };
  }
}
