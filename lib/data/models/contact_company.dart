class ContactCompany {
  final String bs;
  final String catchPhrase;
  final String name;

  const ContactCompany({
    required this.bs,
    required this.catchPhrase,
    required this.name,
  });

  ContactCompany.fromMap(Map<String, Object?>? map)
      : this(
          bs: map?['bs'] as String? ?? '',
          catchPhrase: map?['catchPhrase'] as String? ?? '',
          name: map?['name'] as String? ?? '',
        );

  Map<String, Object?> toMap() {
    return {
      'bs': bs,
      'catchPhrase': catchPhrase,
      'name': name,
    };
  }
}
