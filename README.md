# Contact List Challenge

Flutter challenge retrieving a contact list, and displaying it in two columns on big screens.

## Features

- The app displays a list of users retrieved from [{JSON} Placeholder API](https://jsonplaceholder.typicode.com).
- The avatars for the users are retreived from [Gravatar](https://gravatar.com).
- A list with tappable elements is shown on the first page, displaying a detail page on the next screen on small screens, and aside on big screens.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/contact_list_challenge/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
